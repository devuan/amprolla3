# See LICENSE file for copyright and license details.

"""
Lockfile functions
"""

from contextlib import contextmanager
import fcntl
from time import time

from lib.config import lockpath
from lib.log import die, info


@contextmanager
def amprolla_lock():
    """
    Use the standard library to manage an advisory lock on a lock file.

    This is robust against lingering lock files, process crashing, ...
    """
    with open(lockpath, 'w') as locked_file:
        locked = False
        try:
            fcntl.flock(locked_file, fcntl.LOCK_EX | fcntl.LOCK_NB)
        except OSError:
            info('Lock not obtained. Defering operation.')
        else:
            locked = True
        yield locked


def run_without_lock(f, name, *args, **kwargs):
    """
    Run function f with *args and **kwargs.
    name is only used to log the time it took.
    """
    try:
        t1 = time()
        f(*args, **kwargs)
        t2 = time()
        info('Total {} time: {}'.format(name, (t2 - t1)), tofile=True)
    except Exception as e:
        try:
            import traceback
            with open('amprolla.debug.txt', 'a') as f:
                traceback.print_exc(file=f)
        except:
            pass
        die(e)


def run_with_amprolla_lock(f, name, *args, **kwargs):
    """
    This uses amprolla_lock to ensure that only one amprolla-related
    process is ran at any given time.
    Otherwise it behaves as run_without_lock.
    """
    with amprolla_lock() as locked:
        if locked:
            run_without_lock(f, name, *args, **kwargs)
            return True
        else:
            return False

def run_with_args_locking(f, name, *args, **kwargs):
    """
    Simplify logic in each amprolla step by dealing with sys args and exit
    codes here.
    """
    import sys
    if '--help' in sys.argv or '-h' in sys.argv:
        print("""This is an amprolla step to achieve {step}.

If you run it without arguments, it will use the amprolla lock at:
{lockpath}

You can also run it with --no-lock-I-am-sure as an argument, which will skip
locking.
Notice that if you run multiple amprolla steps simultaneously, this can lead
to a corrupt state, so make sure you have some locking at a different level.
""".format(step=name, lockpath=lockpath))
        sys.exit(0)
    if '--no-lock-I-am-sure' in sys.argv:
        run_without_lock(f, name, *args, **kwargs)
    elif not run_with_amprolla_lock(f, name, *args, **kwargs):
        sys.exit(1)
