amprolla
========

<img src="contrib/amprolla.png" width="64">

amprolla is an apt repository merger originally intended for use with
the [Devuan](https://devuan.org) infrastructure. This version is the
third iteration of the software. The original version of amprolla was
not performing well in terms of speed, and the second version was never
finished - therefore this version has emerged.

amprolla is a tool that will merge a number of different apt-based
repositories into one, while giving control over (not) including given
packages, architectures, or any specific package metadata. Upon completing
the merge, amprolla will generate and optionally create GnuPG signatures
of the according `Release` files.


Dependencies
------------

amprolla requires Python 3, the lowest version it's been tested on was
Python 3.4. It also requires the python-requests library.

### Devuan/Debian

```
rsync gnupg2 python3-requests
```

### Gentoo:

```
net-misc/rsync app-crypt/gnupg dev-python/requests
```


Basic usage
-----------

Copy `lib/config.def.py` to `lib/config.py` and edit `lib/config.py` to
your needs, and then run `amprolla_init.py`. This will download the
repositories we will merge afterwards. When this is done, you can run
`amprolla_merge.py` which will perform the actual merge, and finally
sign the Release files needed. The first time this is done, it is
advisable to run the script found in `contrib/populate_aliases.sh` so
it can fill in the needed symlinks to the different suites. Make sure
you set the correct paths and names in the script.

To perform incremental updates, run `orchestrate.sh` with a cron job
in your desired intervals. Edit the script to set the correct paths.

`nginx` and `lighttpd` configurations can be found in `contrib`.

More documentation should be found in the `doc` directory.


Adding a suite
--------------
If, once set up, you need to add a suite, this general process seems
to work:
- Change the config file
- Run `amprolla_init.py`
- Run something like `cp -a spool/devuan/dists/${SUITEDIR} merged-volatile/dists/${SUITEDIR}`
- Run `amprolla_merge_contents.py`
- Pray to the amprolla gods
- Run `amprolla_merge.py`
- Check that things look alright, specifically that things were indeed
  merged in the suite.
- If sth goes wrong, it might be due to unreasonable assumptions in the code.
  Please do open an issue / shoot an email / ping on IRC

Releasing
---------
- Adapt `lib/config.py` so that all aliases are up to date.
- Check `contrib/populate_aliases.sh` and adapt it if necessary.
- Backup the spool directory: `mv spool spool.old`
- Re-create the spool directory: `amprolla_init.py`
- Ensure there is a suite dir in `merged-volatile` for each suite in `spool`: `for dist in $(ls spool/devuan/dists/); do ls "merged-volatile/dists/${dist}"; done`
  (Watch out for warnings about non-existing directories)
- Proceed as if you were adding a suite (after `amprolla_init.py`)
