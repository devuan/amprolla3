#!/bin/sh
# See LICENSE file for copyright and license details.

# Orchestration of incremental updates

# Make sure these correlate to lib/config.py
AMPROLLA_SOURCE_DIR="${AMPROLLA_SOURCE_DIR:-/home/amprolla/amprolla}"
REPO_ROOT="${REPO_ROOT:-/home/amprolla/amprolla}"
AMPROLLA_LOCK="${AMPROLLA_LOCK:-/run/lock/amprolla.lock}"
RSYNC_URL="${RSYNC_URL:-mirror@pkgmaster.devuan.org:/home/mirror/devuan}"

#
# Pseudo-booleans, will be tested with -n (see test(1)).
# This allows the admin to easily run certain steps or all of them.
#
# Enabled by default, can be disabled in the environment
DO_AMPROLLA_UPDATE="${DO_AMPROLLA_UPDATE:-YES}"
DO_AMPROLLA_PUSH="${DO_AMPROLLA_PUSH:-YES}"
# Disabled by default, can be enabled in the environment
DO_AMPROLLA_MERGE_TRANSLATIONS="${DO_AMPROLLA_MERGE_TRANSLATIONS:-}"
DO_AMPROLLA_MERGE_CONTENTS="${DO_AMPROLLA_MERGE_CONTENTS:-}"
DO_AMPROLLA_MERGE="${DO_AMPROLLA_MERGE:-}"

# Force amprolla_merge if admin requested to run at least one of:
#   amprolla_merge_contents or amprolla_merge_translations
if [ -n "${DO_AMPROLLA_MERGE_CONTENTS}${DO_AMPROLLA_MERGE_TRANSLATIONS}" ]; then
	DO_AMPROLLA_MERGE="YES"
fi

# Don't do anything if we can't lock
flock -n -x "${AMPROLLA_LOCK}" /bin/sh << EOF
# Ensure directories exist
[ -d "${REPO_ROOT}/merged-volatile" ] || mkdir "${REPO_ROOT}/merged-volatile"
[ -d "${REPO_ROOT}/merged-production" ] || mkdir "${REPO_ROOT}/merged-production"

# Each Amprolla step will exit with an error code if something goes wrong
# In that case we should stop the run.
# Notice we locked in this script, so we don't need the amprolla lock
# to run an amprolla step. We can't even get the lock if this code runs.

# amprolla_merge_translations must happen before merge
if [ -n "${DO_AMPROLLA_MERGE_TRANSLATIONS}" ]; then
	python3 "$AMPROLLA_SOURCE_DIR/amprolla_merge_translations.py" --no-lock-I-am-sure || exit 1
fi

# amprolla_merge_contents must happen before merge
if [ -n "${DO_AMPROLLA_MERGE_CONTENTS}" ]; then
	python3 "$AMPROLLA_SOURCE_DIR/amprolla_merge_contents.py" --no-lock-I-am-sure || exit 1
fi

# amprolla_merge must happen after merge_contents and merge_translations
if [ -n "${DO_AMPROLLA_MERGE}" ]; then
	python3 "$AMPROLLA_SOURCE_DIR/amprolla_merge.py" --no-lock-I-am-sure || exit 1
fi

# amprolla_update is the very last step
if [ -n "${DO_AMPROLLA_UPDATE}" ]; then
	python3 "$AMPROLLA_SOURCE_DIR/amprolla_update.py" --no-lock-I-am-sure || exit 1
fi

# Actually propagate any changes
if [ -n "${DO_AMPROLLA_PUSH}" ]; then
	# Switch /merged to merged-volatile
	ln -snf "$REPO_ROOT"/merged-volatile "$REPO_ROOT"/merged

	# Here merged-production is 'dirty' (not necessarily consistent)
	printf "rsyncing volatile to production... "
	rsync --delete -raX "$REPO_ROOT"/merged-volatile/* "$REPO_ROOT"/merged-production
	echo "done!"

	# Switch /merged back to merged-production, as it should be unless synchronising
	ln -snf "$REPO_ROOT"/merged-production "$REPO_ROOT"/merged

	printf "rsyncing production to pkgmaster... "
	rsync --delete -raX \
		"$REPO_ROOT"/merged-production/ "${RSYNC_URL}/merged"
	echo "done!"

	# handle obsolete package logs
	cat "$REPO_ROOT"/log/*-oldpackages.txt | sort | uniq > "$REPO_ROOT"/log/oldpackages.txt

	_logfiles="libsystemd bannedpackages"
	mkdir -p "$REPO_ROOT"/log/t
	for i in \$_logfiles; do
		sort "$REPO_ROOT/log/\${i}.txt" | uniq > "$REPO_ROOT/log/t/\${i}.txt"
	done
	cp -f "$REPO_ROOT"/log/t/*.txt "$REPO_ROOT"/log/

	rsync "$REPO_ROOT"/log/t/*.txt "${RSYNC_URL}"
	rsync "$REPO_ROOT"/log/oldpackages.txt "$REPO_ROOT"/log/amprolla.txt "${RSYNC_URL}"
fi
EOF
